IMAGE ?= registry.gitlab.com/rpdev/docker/qt5-android:latest
help:
	@echo "Targets:"
	@echo "  build - Build the image."
	@echo "  publish - Upload the image to GitLab."

build:
	./run-build.sh ${IMAGE}

publish:
	docker push ${IMAGE}

login:
	docker login registry.gitlab.com
